navigator.geolocation.getCurrentPosition(async position => {
    const url = `http://localhost:3000/weather/${position.coords.latitude},${position.coords.longitude}`
    const fetch_response = await fetch(url)
    const json = await fetch_response.json()
    const lugar = json.timezone
    const Place = lugar.split('/')
    const summary = json.currently.summary
    const temp = json.currently.temperature
    console.log(json)

    document.getElementById('tit').textContent += `${Place[1]}?`
    document.getElementById('info').textContent = `${summary} at ${temp}°F`
    document.getElementById('summ').textContent = `${summary} at ${temp}°F`
})

