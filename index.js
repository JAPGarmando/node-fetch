const express = require('express')
const app = express()
const fetch = require('node-fetch')

app.listen(3000, async () => {
    console.log('Listening on port 3000')

})
app.use(express.static('public'))
app.get('/weather/:latLon', async (request, response) => {
    const latLon = request.params.latLon.split(',')
    const lat = latLon[0]
    const long = latLon[1]
    const url = `https://api.darksky.net/forecast/df0f20a1a519b53e5bef7fcbabd47c78/${lat},${long}`
    const response_fetch = await fetch(url)
    const json = await response_fetch.json()
    console.log(json.latitude)
    response.json(json)

})

